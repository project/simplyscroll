<?php

/**
 * @file views-view-simplyscroll.tpl.php
 *
 */

?>

<div class="<?php print $simplyscroll_container ?> clearfix">
  <ul class="<?php print $customclass ?>">
    <?php foreach ($rows as $row): ?>
      <li><?php print $row ?></li>
    <?php endforeach; ?>
  </ul>
</div>
